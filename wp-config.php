<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'italac');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Bo<vJS`rUNT3&ybJMkz}.}[ghB26>]Ql&+M2;~Hk_`,S]KK0Rd1cS=kT7 IV%>E$');
define('SECURE_AUTH_KEY',  'dy@@QzMyfp|@<|EM,)<n=nx#cUI_-VgNVHaxOIwv8XIfV3@?)H]Za5PiENqScmq~');
define('LOGGED_IN_KEY',    '.GjV6g`}9,l?l.n!%>8F}P&f*=?%$mu5Ql]._r~^2F?(dV9YD4K#wPxKF7M.KYkE');
define('NONCE_KEY',        'qM1zxWO^NH(DiCQ/IG^Cw^?P=nEc{X-b/>BGtCN9^!|) =QYIuCJkfEhRyG`Qn~R');
define('AUTH_SALT',        'wtBxb!( 54%p{<R;:EBlo3eQ!kDK|w;E*^(+vFS7+F!eQF)}!C*[<*1T0*74uh|x');
define('SECURE_AUTH_SALT', 'LSzo?:Xr_C_2dwMaN>02SgUh(l}bdI3r]zzY9SBM$O@&5nryx;.FbuDJ..?[&mv;');
define('LOGGED_IN_SALT',   'd3jjuh#,9n[Gm21AA|ypc>](Wsp/mib%j }=P?L(W2VMH,krW$V8B-T!1+e;IW#g');
define('NONCE_SALT',       '|3dhB`!8eR0,{]NkFyDPT+oh4],T/6jriwajU.&2e;WErqKesS5^EA 3N:W#GamL');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');

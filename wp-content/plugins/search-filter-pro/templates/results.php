<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	while ($query->have_posts())
	{
		$query->the_post();
		/* -------------------------------------------------- */	
		
		$do_not_duplicate[] = get_the_ID();

		$author_id = get_the_author_meta('ID');
		$author = get_the_author();
		$phraseTop = get_field("frase_topo", 'user_' . $author_id);
		$authorTop = get_field("autor_topo", 'user_' . $author_id);
		$topBgColor = get_field('cor_fundo_frase_topo', 'user_' . $author_id);
		$phraseColor = get_field('cor_frase_topo', 'user_' . $author_id);
		$postCustomAuthorAvatar = get_field('user_field_image', 'user_' . get_the_author_meta('ID'));
		$postCustomAuthorAvatarUrl = $postCustomAuthorAvatar['url'];
		$postCustomAuthorAvatarAlt = $postCustomAuthorAvatar['alt'];
		$footerButtonTarget = get_field('page_about_button_target');
		$author_link = get_author_posts_url( get_the_author_meta( 'ID' ) );

		$positivo = array();
		$negativo = array();
		$positivoNumber = "";
		$negativoNumber = "";

		$avaliacoes = get_posts(array(
			'post_type' => 'avaliacoes',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'relacao_autor',
					'value' => $author_id
					)
				)
			));

			$positivoNumber = count($positivo);
			$negativoNumber = count($negativo);
			

			//pega o título dos serviços para usar 
			$title = get_the_title();  ?>

		
			<div class="col-3 item-recipes">
				<div class="figure-recipes"  id="post-<?php the_ID(); ?>" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg);"></div>
				<div class="content-info">
					<?php the_title( sprintf( '<h3><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
					<ul class="icons-recipes">
						<li class="difficulty-fild">
							<strong>Dificuldade</strong>
							<?php the_field('dificuldade'); ?>	
						</li>
						<li class="preparation-fild">
							<strong>Preparo</strong>
							<?php the_field('tempo'); ?>
						</li>
						<li class="yield-fild">
							<strong>Rendimento</strong>
							<?php the_field('porções'); ?>
						</li>
					</ul>
					<div class="news-btn">
						<a href="<?php the_permalink() ?>" class="btn btn-blue"  onclick="_gaq.push(['_trackEvent', 'Receitas', 'Filtro de receitas', 'Acessou <?php the_title(); ?>']);">Ver receita</a>
					</div>
				</div>
			</div>
		<?php //$indexControl++; ?>


		<?php
		/* --------------------------------------------- */
	}
	?>

	<div class="pagination">

		<div class="nav-previous"><?php next_posts_link( 'Outras receitas', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Receitas mais recentes' ); ?></div>
		<?php
		/* example code for using the wp_pagenavi plugin */
		if (function_exists('wp_pagenavi'))
		{
			echo "<br />";
			wp_pagenavi( array( 'query' => $query ) );
		}
		?>
	</div>
	<?php
}
else
{
	echo "No Results Found";
}
?>
<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

get_header(); ?>


	<div id="primary recipe-main" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<header class="page-header" style="background-image: url('<?php get_site_url() ?>/wp-content/themes/italac/images/banner-receitas.jpg')">
				<div class="container">
					<!-- <h1>Receitas</h1> -->
				</div>
			</header><!-- .page-header -->
			<div class="img-mobile">
				<img src="/wp-content/uploads/2019/11/Italac-Shippou-Banner-Site-3150x480.jpg" />
			</div>
			<div class="section-main">
				<div class="container">
					<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?> 
					<div class="recipe-filter">
						<?php echo  do_shortcode('[searchandfilter slug="filtro-de-receitas"]'); ?>
					</div>
				</div>

				<div class="container">
					<?php echo  do_shortcode('[searchandfilter slug="filtro-de-receitas" show="results"]'); ?>
					<div id="result"></div>

					<h2 class="text-center main-title">As mais acessadas</h2>
					<div class="row recipes-box">					
					<?php
						$posts = wp_most_popular_get_popular( array( 'limit' => 4, 'post_type' => 'receitas', 'range' => 'all_time' ) );
						global $post;
						if ( count( $posts ) > 0 ): foreach ( $posts as $post ):
							setup_postdata( $post );
							?>
							<div class="col-3 item-recipes">
								<div class="figure-recipes" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg););"></div>
								<div class="content-info">
									<h3><?php the_title(); ?></h3>
									<ul class="icons-recipes">
										<li class="difficulty-fild">
											<strong>Dificuldade</strong>
											<?php the_field('dificuldade'); ?>	
										</li>
										<li class="preparation-fild">
											<strong>Preparo</strong>
											<?php the_field('tempo'); ?>
										</li>
										<li class="yield-fild">
											<strong>Rendimento</strong>
											<?php the_field('porções'); ?>
										</li>
									</ul>
									<div class="news-btn">
										<a href="<?php the_permalink() ?>" class="btn btn-blue" onclick="_gaq.push(['_trackEvent', 'Receitas', 'As mais acessadas', 'Acessou <?php the_title(); ?>']);">Ver receita</a>
									</div>
								</div>
							</div>
							<?php
						endforeach; endif;
						?>
					</div>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>

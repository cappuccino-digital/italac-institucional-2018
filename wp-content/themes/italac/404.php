<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<div class="container">
						<h1 class="text-center"><?php _e( 'Página não encontrada.', 'Italac' ); ?></h1>
					</div>
				</header><!-- .page-header -->
			</section><!-- .error-404 -->
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>

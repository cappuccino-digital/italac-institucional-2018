<?php
/**
 * The template for displaying marca Italakinho
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area gradiend-section">
			<?php if ( have_posts() ) : ?>
			<div class="news-section section-main">
				<div class="container">
					<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?>
					
					<?php echo '<h1 class="text-center">';single_cat_title();echo '</h1>'; ?>
					<div class="row italakinho-section">
						<div class="col-sm-12 col-xl-3 col-md-12 text-center foguete-box">
							<img src="<?php get_site_url() ?>/wp-content/themes/italac/images/nave-italakinho.png" alt="Imagem de um foguete">
						</div>
						<div class="col-sm-12 col-xl-6 col-md-12 prod-italakinho-box">
							<div class="row prod-box">
								<?php
								// Start the Loop.
								while ( have_posts() ) : the_post();
									/*
									* Include the Post-Format-specific template for the content.
									* If you want to override this in a child theme, then include a file
									* called content-___.php (where ___ is the Post Format name) and that will be used instead.
									*/
									get_template_part( 'template-parts/content', 'produtos-italakinho' );
									
								// End the loop.
								endwhile;
								
								else :
									get_template_part( 'template-parts/content', 'none' );

								endif;
								?>
							</div>
						</div>
						<div class="col-sm-12 col-xl-3 col-md-12  text-center astronauta-box">
							<img src="<?php get_site_url() ?>/wp-content/uploads/2018/10/Image-89.png" alt="Imagem de um astronauta">
						</div>
					</div>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>

<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>


<div class="col-3 item-recipes">

    <div class="figure-recipes"  id="post-<?php the_ID(); ?>" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg);"></div>
    <div class="content-info">
        <?php the_title( sprintf( '<h3><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
        <ul class="icons-recipes">
            <li class="difficulty-fild">
                <strong>Dificuldade</strong>
                <?php the_field('dificuldade'); ?>	
            </li>
            <li class="preparation-fild">
                <strong>Preparo</strong>
                <?php the_field('tempo'); ?>
            </li>
            <li class="yield-fild">
                <strong>Rendimento</strong>
                <?php the_field('porções'); ?>
            </li>
        </ul>
        <div class="news-btn">
            <a href="<?php the_permalink() ?>" class="btn btn-blue" onclick="_gaq.push(['_trackEvent', 'Receitas', 'Listagem de receitas', 'Acessou <?php the_title(); ?>']);">Ver receita</a>
        </div>
    </div>
</div>



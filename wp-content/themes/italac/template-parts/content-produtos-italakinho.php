<?php
/**
 * The template used for displaying product content in looping categoria.
 *
 * @package Italac
 * @since 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" class="col-sm-4 col-xl-4 col-md-4 prod-single">
	<a href="<?php the_permalink(); ?>"  onclick="_gaq.push(['_trackEvent', 'Produto Italakinho', 'Listagem de produtos', 'Acessou <?php the_title(); ?>']);">
	<?php
	if (get_the_post_thumbnail() !== ''):
		the_post_thumbnail();
	else:
		echo '<img class="" src="/wp-content/themes/italac/images/prod-cover.jpg" alt="" />';
	endif;
	?>
	</a>
	
	<a href="<?php the_permalink(); ?>" class="btn btn-gray"  onclick="_gaq.push(['_trackEvent', 'Produto Italakinho', 'Listagem de produtos', 'Acessou <?php the_title(); ?>']);"><?php the_title(); ?></a>
	
</div>
<?php
/**
 * The template part for displaying single Recipes
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/banner-recipes.jpg);" >
	</header><!-- .entry-header -->

	<?php Italac_excerpt(); ?>
	<div class="entry-content">
		<div class="container">
            
			<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?>
            <h1><?php the_title(); ?></h1>
			<div class="small-container">
            <?php if ( is_singular() ) : ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>    
                    <!-- Icons top recipes -->
                    <ul class="icons-recipe-top">
                        <li class="icon-recipe">
                            <img src="<?php get_site_url() ?>/wp-content/themes/italac/images/difficult-2-ico.png" alt="">
                            <strong>Dificuldade</strong>
                            <span><?php the_field( 'dificuldade' ); ?></span>
                        </li>

                        <li class="icon-recipe">
                            <img src="<?php get_site_url() ?>/wp-content/themes/italac/images/preparation-2-ico.png" alt="">
                            <strong>Preparo</strong>
                            <span><?php the_field( 'tempo' ); ?></span>
                        </li>

                        <li class="icon-recipe">
                            <img src="<?php get_site_url() ?>/wp-content/themes/italac/images/yiel-2-ico.png" alt="">
                            <strong>Rendimento</strong>
                            <span><?php the_field( 'porções' ); ?></span>
                        </li>
                    </ul>

                    <div class="content-main">
                        <!-- Coteudo principal -->
                        <div class="content-text">
                            <h2>Ingredientes</h2>
                            <?php the_field( 'texto_de_ingredientes' ); ?>
                        </div>

                        <!-- Produtos relacionados -->
                        <div class="products-box">
                            <div class="products-inter">
                                <?php
                                    $produtos = get_field('produtos');
                                    if( $produtos ):
                                ?>
                                <?php foreach( $produtos[0]['produto'] as $produto ): ?>
                                <a href="<?php echo get_permalink( $produto->ID ); ?>"  onclick="_gaq.push(['_trackEvent', 'Receita', 'Produto da receita', 'Acessou <?php echo get_the_title( $produto->ID ); ?>']);">
                                    <div id="post-<?php echo $produto->ID; ?>" class="item-products">
                                        <?php 
                                            $imgProd = get_field('imagem_do_produto' , $produto->ID); 
                                            if( $imgProd ):
                                                echo "<figure><img src='$imgProd' alt='' width='100%'></figure>";
                                            else:
                                                echo '<figure><img class="" src="/wp-content/themes/italac/images/prod-cover.jpg" alt="" /></figure>';
                                            endif; 
                                        ?>
                                        <h3><?php echo get_the_title( $produto->ID ); ?></h3>
                                    </div>
                                </a>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <h2>Modo de Preparo</h2>
                    <?php the_content(); ?>

                    <!-- Categorias -->
                    <!--
                    <?php if ( in_array( 'categoria-receitas', get_object_taxonomies( get_post_type() ) ) ) : ?>
                        <span class="cat-links"><?php echo __( 'Categorias:', 'odin' ) . ' ' . get_the_term_list( '', 'categoria-receitas', '', ', ' ); ?></span>
                    <?php endif; ?>
                    -->

                    <!-- Vídeo do Youtube -->
                    <!-- Vídeo do Youtube:<br />
                    <?php the_field('video_do_youtube'); ?>
                    -->

                    <!-- Informações adicionais -->
                    <h3>Informações adicionais:</h3> 
                    <p><?php echo get_post_meta( get_the_ID(), 'informações_adicionais', true ); ?></p>
                    <p>
                        <a class="btn btn-blue  btn-facebook" onclick="javascript: window.open('https://www.facebook.com/dialog/share?app_id=678864479180521&display=popup&href=' + window.location, 'Compartilhar', 'width=660,height=330 '); void(0);  _gaq.push(['_trackEvent', 'Receitas', 'Compartilhou', '<?php the_title(); ?>']);" ><i class="fa fa-facebook"></i>Compartilhar</a>
                    </p>
                    <!-- Btn Imprimir Receita -->
                    <?php echo  do_shortcode('[print-me target="#print_box"]'); ?>

                    <div id="print_box" style="font-family: 'breeregular', sans-serif; color:#11357e;" color="#11357e">
                        <div align="center">
                            <table border="0" width="100%">
                                <tr>
                                    <td align="center"><img src="/wp-content/uploads/2018/08/logo.png" alt=""></td>
                                </tr>
                            </table>
                        </div>
                        <br>
                        <br>
                        <table border="0" style="font-family: 'breeregular', sans-serif; color:#11357e;" color="#11357e" width="100%">
                            <tr>
                                <td align="center">
                                    <img src="<?php get_site_url() ?>/wp-content/themes/italac/images/difficult-2-ico.png" width="90px" alt=""><br>
                                    <strong>Dificuldade</strong><br>
                                    <span><?php the_field( 'dificuldade' ); ?></span>
                                </td>
                                <td align="center">
                                    <img src="<?php get_site_url() ?>/wp-content/themes/italac/images/preparation-2-ico.png" width="90px" alt=""><br>
                                    <strong>Preparo</strong><br>
                                    <span><?php the_field( 'tempo' ); ?></span>
                                </td>
                                <td align="center">
                                    <img src="<?php get_site_url() ?>/wp-content/themes/italac/images/yiel-2-ico.png" width="90px" alt=""><br>
                                    <strong>Rendimento</strong><br>
                                    <span><?php the_field( 'porções' ); ?></span>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <h1><?php the_title(); ?></h1>
                        <h2>Ingredientes</h2>
                        <?php the_field( 'texto_de_ingredientes' ); ?>
                        <h2>Modo de Preparo</h2>
                        <?php the_content(); ?>
                        <h3>Informações adicionais:</h3> 
                        <p><?php echo get_post_meta( get_the_ID(), 'informações_adicionais', true ); ?></p>
                    </div>
                    
                </article>
                <?php endif; ?>
            </div>
		</div>
    </div><!-- .entry-content -->
    <!-- Related Recipes -->
    
    <?php
        // get the custom post type's taxonomy terms
        $custom_taxterms = wp_get_object_terms( $post->ID, 'tipo_de_receita', array('fields' => 'ids') );

        $args = array(
            'post_type' => 'receitas',
            'post_status' => 'publish',
            'posts_per_page' => 4, // you may edit this number
            'orderby' => 'rand',
            'post__not_in' => array ( $post->ID ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'tipo_de_receita',
                    'field' => 'id',
                    'terms' => $custom_taxterms
                )
            )
        );
        $related_items = new WP_Query( $args );
        // loop over query
        if ( $related_items->have_posts() ) : ?>
        <div class="related-recipes">
            <div class="container">
                <h2 class="text-center main-title">Receitas Relacionadas</h2>
                <div class="row recipes-box">
                    <?php while ( $related_items->have_posts() ) : $related_items->the_post(); ?>
                    <div class="col-3 item-recipes">
                        <div class="figure-recipes" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg););"></div>
                        <div class="content-info">
                            <h3><?php the_title(); ?></h3>
                            <ul class="icons-recipes">
                                <li class="difficulty-fild">
                                    <strong>Dificuldade</strong>
                                    <?php the_field('dificuldade'); ?>	
                                </li>
                                <li class="preparation-fild">
                                    <strong>Preparo</strong>
                                    <?php the_field('tempo'); ?>
                                </li>
                                <li class="yield-fild">
                                    <strong>Rendimento</strong>
                                    <?php the_field('porções'); ?>
                                </li>
                            </ul>
                            <div class="news-btn">
                                <a href="<?php the_permalink() ?>" class="btn btn-blue">Ver receita</a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
        
                </div>
            </div>
        </div>            
        <?php else : ?>
        <div class="related-recipes">
            <div class="container">
                <h2 class="text-center main-title">Outras Receitas</h2>
                <div class="row recipes-box">
                    <?php $loop = new WP_Query( array( 'post_type' => 'receitas', 'posts_per_page' => 4 ) );?>
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="col-3 item-recipes">
                        <div class="figure-recipes" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg););"></div>
                        <div class="content-info">
                            <h3><?php the_title(); ?></h3>
                            <ul class="icons-recipes">
                                <li class="difficulty-fild">
                                    <strong>Dificuldade</strong>
                                    <?php the_field('dificuldade'); ?>	
                                </li>
                                <li class="preparation-fild">
                                    <strong>Preparo</strong>
                                    <?php the_field('tempo'); ?>
                                </li>
                                <li class="yield-fild">
                                    <strong>Rendimento</strong>
                                    <?php the_field('porções'); ?>
                                </li>
                            </ul>
                            <div class="news-btn">
                                <a href="<?php the_permalink() ?>" class="btn btn-blue">Ver receita</a>
                            </div>
                        </div>
                    </div>
                    <?php 
                        endwhile;
                        wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
        <?php endif;
        // Reset Post Data
        wp_reset_postdata();
    ?> 
        
</article><!-- #post-## -->




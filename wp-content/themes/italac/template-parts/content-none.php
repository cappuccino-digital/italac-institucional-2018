<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>

<section class="no-results not-found">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
		<header class="page-header">
			<div class="container">
				<h1 class="text-center">Página não encontrada</h1>
			</div>
		</header>
	<?php elseif ( is_search() ) : ?>
		<header class="page-header">
			<div class="container">
				<h1 class="text-center">Resultado não encontrado</h1>
			</div>
		</header>
	<?php else : ?>
		
		<div class="container section-main">	
			<h1 class="text-center">Não existe conteúdo para essa página</h1>
		</div>
		
	<?php endif; ?>
</section><!-- .no-results -->

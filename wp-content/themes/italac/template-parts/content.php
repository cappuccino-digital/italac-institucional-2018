<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>

<div class="col-3 news-item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php the_permalink() ?>" class="image-news" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg); display:block;"></a>
		<div class="content-new">
		<?php the_title( sprintf( '<h3><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
			<div class="news-btn">
				<a href="<?php the_permalink() ?>" class="btn btn-white">Ver notícia</a>
			</div>
		</div>
	</article>
</div>
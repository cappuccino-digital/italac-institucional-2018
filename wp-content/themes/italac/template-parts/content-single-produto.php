<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="product-section entry-content">
				<div class="section-main only-desktop">
					<div class="container">
						<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?>
						<h1 class="text-center"><?php the_title(); ?></h1>
					</div>

					<div class="border-section">
						<div class="container">
							<h2 class="text-center">Informações Nutricionais</h2>
							<div class="row">
								<div class="col-4 grid-box">
									<div class="bottom-talac">
										<div class="product-box">
											<figure>
											<?php
												if (get_the_post_thumbnail() !== ''):
													the_post_thumbnail();
												else:
													echo '<img class="" src="/wp-content/themes/italac/images/prod-cover.jpg" alt="" />';
												endif;
											?>											
											</figure>
											<!-- Download da imagem do produto -->
											<?php if (get_the_post_thumbnail() !== ''): ?>	
												<a target="_blank" href="<?php the_post_thumbnail_url(); ?>" download  class="btn-download" onclick="_gaq.push(['_trackEvent', 'Produto', 'Download', '<?php the_title(); ?>']);">Download</a>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="col-4 grid-box">
									<div class="bottom-talac">
										<div class="nutriction-table">
											<?php if( have_rows('tabela_nutricional') ): ?>
											<h4 class="text-center"><?php echo get_post_meta( get_the_ID(), 'porção', true ); ?></h4>
											<div class="amount-per-serving">
												<table class="table">
													<tr>
														<td>
															Quantidade por porção
														</td>
														<td class="text-right">
															% VD (*)
														</td>
													</tr>
												</table><!-- .amount-per-serving -->
											</div>
											<div class="ingredients">
												<table class="table table-hover">
													<?php while ( have_rows('tabela_nutricional') ) : the_row(); ?>
														<tr>
															<td><?php the_sub_field('ingrediente'); ?></td>
															<td><?php the_sub_field('quantidade'); ?></td>
															<td><?php the_sub_field('valor_diario'); ?></td>
														</tr>
													<?php endwhile; ?>
												</table>
											</div><!-- .ingredients -->
										</div>

										<div class="table-description text-justify">
											<br>
											<?php echo get_post_meta( get_the_ID(), 'descrição_da_tabela', true ); ?>
										</div><!-- .table-description -->
										<?php else : ?>
											<h3 class="text-center">Não há tabela nutricional</h3>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-4 grid-box">
									<div class="bottom-talac">
										<div class="recipe-box">
											<?php
												$receitas = get_field('receita');
												if( $receitas ):
											?>
											<?php foreach( $receitas[0]['receita'] as $receita ): ?>
												<?php
												if ( get_the_post_thumbnail( $receita->ID) !== ''):
													$url_thumb = get_the_post_thumbnail_url( $receita->ID);
													echo "<div class='figure-recipes' style='background-image: url($url_thumb);'></div>";
												else:
													echo '<div class="figure-recipes" style="background-image: url(/wp-content/uploads/2018/09/banner-recipes.jpg);"></div>';
												endif;
												?>
												<h2><?php echo get_the_title( $receita->ID ); ?></h2>
												<a href="<?php echo get_permalink( $receita->ID ); ?>" class="btn btn-blue"  onclick="_gaq.push(['_trackEvent', 'Produto', 'Ver Receita', '<?php the_title(); ?>']);">Ver Receita</a>
											<?php endforeach; ?>
											<?php else: ?>
												<div class="figure-recipes" style="background-image: url('<?php get_site_url() ?>/wp-content/uploads/2018/09/banner-recipes.jpg');"></div>
												<h2>Veja as receitas utilizando produtos Italac</h2>
												<a href="<?php get_site_url() ?>/receitas" class="btn btn-blue" onclick="_gaq.push(['_trackEvent', 'Produto', 'Ver Receitas', 'Todas receitas'" >Ver Receitas</a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="products-info">
						<div class="container">
							<div class="row">
								<div class="col-4 grid-box">
									<h3>Embalagens e Códigos</h3>
									<?php the_content(); ?>
								</div>
								<div class="col-4 grid-box">
									<?php 
										$ingredientes = get_post_meta( get_the_ID(), 'ingredientes', true );
										if ($ingredientes !== '') {
											echo '<h3>Ingredientes</h3>';
											echo $ingredientes;
										}
									?>
								</div>
								<div class="col-4 grid-box">
									<?php 
										$frases = get_post_meta( get_the_ID(), 'frases_de_advertencia', true );
										if ($frases !== '') {
											echo '<h3>Frases de Advertência</h3>';
											echo $frases;
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="only-mobile">
					<div class="container">
						<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?>
						<h1 class="text-center"><?php the_title(); ?></h1>
					</div>

					<div class="border-section">
						<div class="container">
							<div class="bottom-talac">
								<div class="product-box">
									<figure>
										<?php
										if (get_the_post_thumbnail() !== ''):
											the_post_thumbnail();
										else:
											echo '<img class="" src="/wp-content/themes/italac/images/prod-cover.jpg" alt="" />';
										endif;
										?>											
									</figure>
									
									<!-- Download da imagem do produto -->
									<?php if (get_the_post_thumbnail() !== ''): ?>	
										<a target="_blank" href="<?php the_post_thumbnail_url(); ?>" download  class="btn-download"  onclick="_gaq.push(['_trackEvent', 'Produto', 'Download', '<?php the_title(); ?>']);">Download</a>
									<?php endif; ?>
									<br><br>
								</div>
							</div>

							<div class="bottom-talac">
								<div class="nutriction-table">
									<?php if( have_rows('tabela_nutricional') ): ?>
									<h4 class="text-center"><?php echo get_post_meta( get_the_ID(), 'porção', true ); ?></h4>
									<div class="amount-per-serving">
										<table class="table">
											<tr>
												<td>
													Quantidade por porção
												</td>
												<td class="text-right">
													% VD (*)
												</td>
											</tr>
										</table><!-- .amount-per-serving -->
									</div>
									<div class="ingredients">
										<table class="table table-hover">
											<?php while ( have_rows('tabela_nutricional') ) : the_row(); ?>
												<tr>
													<td><?php the_sub_field('ingrediente'); ?></td>
													<td><?php the_sub_field('quantidade'); ?></td>
													<td><?php the_sub_field('valor_diario'); ?></td>
												</tr>
											<?php endwhile; ?>
										</table>
									</div><!-- .ingredients -->
								</div>

								<div class="table-description text-justify">
									<br>
									<?php echo get_post_meta( get_the_ID(), 'descrição_da_tabela', true ); ?>
								</div><!-- .table-description -->
								<?php else : ?>
									<h3 class="text-center">Não há tabela nutricional</h3>
								</div>
								<?php endif; ?>
							</div>

							<div><!-- Ingredientes -->
								<?php 
									$ingredientes = get_post_meta( get_the_ID(), 'ingredientes', true );
									if ($ingredientes !== '') {
										echo '<h3>Ingredientes</h3>';
										echo $ingredientes;
									}
								?>
							</div>
							<br>
							<div><!-- Frases de Advertência  -->
								<?php 
									$frases = get_post_meta( get_the_ID(), 'frases_de_advertencia', true );
									if ($frases !== '') {
										echo '<h3>Frases de Advertência</h3>';
										echo $frases;
									}
								?>
							</div>
							<br>
							<div class="col-4 grid-box">
								<h3>Embalagens e Códigos</h3>
								<?php the_content(); ?>
							</div>
							<div class="bottom-talac"><!-- REceita -->
								<div class="recipe-box">
									<?php
										$receitas = get_field('receita');
										if( $receitas ):
									?>
									<?php foreach( $receitas[0]['receita'] as $receita ): ?>
										<?php
										if ( get_the_post_thumbnail( $receita->ID) !== ''):
											$url_thumb = get_the_post_thumbnail_url( $receita->ID);
											echo "<div class='figure-recipes' style='background-image: url($url_thumb);'></div>";
										else:
											echo '<div class="figure-recipes" style="background-image: url(/wp-content/uploads/2018/09/banner-recipes.jpg);"></div>';
										endif;
										?>
										<h2><?php echo get_the_title( $receita->ID ); ?></h2>
										<a href="<?php echo get_permalink( $receita->ID ); ?>"  onclick="_gaq.push(['_trackEvent', 'Produto', 'Ver Receita', '<?php the_title(); ?>']);" class="btn btn-blue" >Ver Receita</a>
									<?php endforeach; ?>
									<?php else: ?>
										<div class="figure-recipes" style="background-image: url('<?php get_site_url() ?>/wp-content/uploads/2018/09/banner-recipes.jpg');"></div>
										<h2>Veja as receitas utilizando produtos Italac</h2>
										<a href="<?php get_site_url() ?>/receitas" class="btn btn-blue"   onclick="_gaq.push(['_trackEvent', 'Produto', 'Ver Receita', 'Todas receitas']);">Ver Receitas</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="products-section section-main">
				<div class="container">
					<h2 class="text-center main-title"><span class="small">Nossos</span> Produtos</h2>
					<div class="slider-products">
						<?php echo do_shortcode("[rev_slider alias='products-carrousel']"); ?>
					</div>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>
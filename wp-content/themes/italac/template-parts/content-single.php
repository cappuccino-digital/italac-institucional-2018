<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<style>
		.entry-header{
			background-image: url('/wp-content/themes/italac/images/headers/novidades-header.jpg');
		}
		.category-saudabilidade .entry-header{
			background-image: url('/wp-content/themes/italac/images/headers/banner-saudabilidade.jpg');
		}
	</style>
	<header class="entry-header">
		<div class="container"></div>
	</header><!-- .entry-header -->

	<?php Italac_excerpt(); ?>
	<div class="entry-content">
		<div class="container">
			<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?>
			<div class="small-container">	
				<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
				<p class="date-post"><?php the_time('j'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?></p>	
				<?php
					the_content();
				?>
				<p>
					<a class="btn btn-blue btn-facebook" onclick="javascript: window.open('https://www.facebook.com/dialog/share?app_id=678864479180521&display=popup&href=' + window.location, 'Compartilhar', 'width=660,height=330 '); void(0);  _gaq.push(['_trackEvent', 'Blog', 'Compartilhou', '<?php the_title(); ?>']);" ><i class="fa fa-facebook"></i>Compartilhar</a>
				</p>
			</div>
		</div>
	</div><!-- .entry-content -->
	<div class="container">
		<div class="news-section">
			<div class="container">
				<div class="row news-box">
					<?php $news_home = new WP_Query( 'posts_per_page=4' ); ?>
						<?php while ($news_home -> have_posts()) : $news_home -> the_post(); ?>
						
						<div class="col-3 news-item">
							<article>
								<div class="image-news" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg);"></div>
								<div class="content-new">
									<h3><?php the_title(); ?></h3>
									<div class="news-btn">
										<a href="<?php the_permalink() ?>" class="btn btn-white">Ver notícia</a>
									</div>
								</div>
							</article>
						</div>
						<?php 
						endwhile;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
	</div>
</article><!-- #post-## -->

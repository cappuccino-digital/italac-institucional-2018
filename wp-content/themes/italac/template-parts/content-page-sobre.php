<?php
	/**
	* Template Name: Sobre a Italac
	*
	* @package WordPress
	* @subpackage Italac
	* @since Italac 1.0
	*/

	get_header();
?>

<div id="sobre-a-italac" class="content-area">
	<main id="about" class="site-main content" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
				<div class="container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>
			</header><!-- .entry-header -->

			<div class="container">
				<div class="entry-content">
					<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?>
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
					?>
					<!-- Timeline -->
					<div class="timeline-box">
						<?php echo do_shortcode("[timeline-history]"); ?>
					</div>
				</div>
			</div>

			<!-- Center Banner -->
			<div class="sobre-center-banner">
				<div class="container">
					<h2>Italac, a marca de lácteos mais comprada do Brasil*<br> está no TOP 3 das marca de bens de consumo<br> mais escolhida pelos brasileiros!</h2>
					<p><small>*Fonte: Kantar divisão Worldpanel, Brand Footprint 2020</small></p>
					<ul>
						<li>+ de 100 produtos</li>
						<li>+ de 20 mil pontos de vendas </li>
						<li>+ de 7 milhões de litros por dia <span>Capacidade de produção instalada</span> </li>
					</ul>
				</div>		
			</div>

			<div class="mission-vision-values section-main entry-content">
				<!-- <div class="container">
					<h2 class="text-center main-title">Missão, visão e valores</h2>
				</div> -->
				<div class="content-mission-vision-values">
					<div class="container">
						<div class="itens-main row">
							<div class="item-inter col-4">
								<h3>Missão</h3>
								<div class="inter-text">
									<p>
									<?php
										while ( have_posts() ) : the_post();
										the_field( 'missão' ); 
										endwhile;
									?>
									</p>
								</div>
							</div>

							<div class="item-inter col-4">
								<h3>Visão</h3>
								<div class="inter-text">
									<p>
									<?php 
										while ( have_posts() ) : the_post();
										the_field( 'visão' ); 
										endwhile;
									?>
									</p>
								</div>
							</div>

							<div class="item-inter col-4">
								<h3>Valores</h3>
								<div class="inter-text">
								<?php 
										while ( have_posts() ) : the_post();
										the_field( 'valores' ); 
										endwhile;
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="units-section entry-content">
				<div class="container section-main">
					<h2 class="text-center main-title">Unidades</h2>
					<div class="text-center">
					<?php 
						while ( have_posts() ) : the_post();
						the_field( 'texto_unidades' ); 
						endwhile;
					?>
					</div>
					<div class="unit-slide">
						<?php echo do_shortcode('[metaslider id="1693"]'); ?>
					</div>
				</div>
			</div>
		</article><!-- #post-## -->
	</main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>




<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>
<div class="col-sm-12 col-xl-2 col-md-12 src-item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a href="<?php the_permalink() ?>">
		<?php
			if ( get_the_post_thumbnail() !== ''):
				$url_thumb = get_the_post_thumbnail_url();
				echo "<div class='image-news image-src' style='background-image: url($url_thumb);' ></div>";
			else:
				echo "<div class='image-news image-src' style='background-image: url(/wp-content/uploads/2018/09/Italac.jpg);'></div>";
			endif;
		?></a>
		<h3><?php the_title(); ?></h3>
		<div class="src-content"><?php the_excerpt(); ?></div>
		<div class="text-center"><a href="<?php the_permalink() ?>" class="btn btn-blue">Saiba mais</a></div>
	</article>
</div>


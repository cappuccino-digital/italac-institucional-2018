<?php
/**
 * The template used for displaying product content in looping categoria.
 *
 * @package Italac
 * @since 1.0.0
 */
	
?>

<div id="post-<?php the_ID(); ?>" class="col-sm-4 col-xl-2 col-md-4 prod-single">
	<div class="only-desktop">
		<a href="<?php the_permalink(); ?>" onclick="_gaq.push(['_trackEvent', 'Produto <?php echo single_cat_title(); ?>', 'Listagem de produtos', 'Acessou <?php the_title(); ?>']);">
		<?php 
			$sub_field_img = get_field('imagem_do_produto');
			if (!empty($sub_field_img)) {
			echo '<img src="' . $sub_field_img . '" alt="" />';
			} else{
				echo '<img src="/wp-content/themes/italac/images/prod-cover-square.jpg">';
			}
		?>
		</a>
	</div>
	<div class="only-mobile">
		<a href="<?php the_permalink(); ?>" onclick="_gaq.push(['_trackEvent', 'Produto <?php echo single_cat_title(); ?>', 'Listagem de produtos', 'Acessou <?php the_title(); ?>']);">
		<?php
		if (get_the_post_thumbnail() !== ''):
			the_post_thumbnail();
		else:
			echo '<img class="" src="/wp-content/themes/italac/images/prod-cover.jpg" alt="" />';
		endif;
		?>
		</a>
	</div>
	
	<a href="<?php the_permalink(); ?>" class="btn btn-gray" onclick="_gaq.push(['_trackEvent', 'Produto <?php echo single_cat_title(); ?>', 'Listagem de produtos', 'Acessou <?php the_title(); ?>']);"><?php the_title(); ?></a>
	
</div>

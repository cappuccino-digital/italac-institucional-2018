<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
		<div class="container">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header><!-- .entry-header -->

	<div class="container">
		
		<div class="entry-content">
			<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?> 
			<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'Italac' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'Italac' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
			?>
		</div><!-- .entry-content -->
	</div>
</article><!-- #post-## -->

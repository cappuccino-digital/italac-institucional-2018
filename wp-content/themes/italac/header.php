<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="<?php get_site_url() ?>/wp-content/themes/italac/images/favicon.ico" rel="shortcut icon" />
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
			appId            : '678864479180521',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.2'
			});
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118220634-1"></script>
		<script>
			// Google Analytics
			var _gaq = _gaq || [];
				_gaq.push(['_setAccount', 'UA-118220634-1']);
				_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s) {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '2777705262455474');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none" 
		src="https://www.facebook.com/tr?id=2777705262455474&ev=PageView&noscript=1
		https://www.facebook.com/tr?id=2777705262455474&ev=PageView&noscript=1
		" 
		/></noscript>
		<!-- End Facebook Pixel Code -->
</head>



<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="site-inner">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'Italac' ); ?></a>

		<header id="masthead" class="site-header header" role="banner">
			<div class="container">
				<div class="site-header-main row">
					<div class="site-branding col-2">
						<?php Italac_the_custom_logo(); ?>
					</div><!-- .site-branding -->

					<?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>
						<div id="site-header-menu" class="site-header-menu col-10  navbar-expand-lg navbar-light">
							<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
							</button>
							<div class="collapse navbar-collapse content-navs" id="navbarSupportedContent">
								<nav id="site-navigation"  class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'Italac' ); ?>">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'primary',
											'menu_class'     => 'primary-menu navbar-nav mr-auto',
										) );
									?>
								</nav><!-- .main-navigation -->
								<?php endif; ?>

								<?php if ( has_nav_menu( 'social' ) ) : ?>
								<nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'Italac' ); ?>">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'social',
											'menu_class'     => 'social-links-menu',
											'depth'          => 1,
											'link_before'    => '<span class="screen-reader-text">',
											'link_after'     => '</span>',
										) );
									?>
								</nav><!-- .social-navigation -->
								<?php endif; ?>
											
								<!--<div class="header-search"></div>
								<div class="header-search-inter">
									<?php get_search_form(); ?>
									<a href="#" class="close-form">X</a>
								</div>-->
							</div>
						</div><!-- .site-header-menu -->
					<?php endif; ?>
				</div><!-- .site-header-main -->
				<?php if ( get_header_image() ) : ?>
					<?php
						/**
						 * Filter the default Italac custom header sizes attribute.
						 *
						 * @since Italac 1.0
						 *
						 * @param string $custom_header_sizes sizes attribute
						 * for Custom Header. Default '(max-width: 709px) 85vw,
						 * (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px'.
						 */
						$custom_header_sizes = apply_filters( 'Italac_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
					?>
					<div class="header-image">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php header_image(); ?>" srcset="<?php echo esc_attr( wp_get_attachment_image_srcset( get_custom_header()->attachment_id ) ); ?>" sizes="<?php echo esc_attr( $custom_header_sizes ); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
						</a>
					</div><!-- .header-image -->
				<?php endif; // End header image check. ?>
			</div>
		</header><!-- .site-header -->
		
		<div class="site-content">
		
<?php
/**
 * Template for displaying search forms in Italac
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'Italac' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Pesquisar &hellip;', 'placeholder', 'Italac' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo _x( 'Buscar', 'submit button', 'Italac' ); ?></span></button>
</form>

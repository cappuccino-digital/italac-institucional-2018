<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container">
				<div class="border-info-footer">
					O MINISTÉRIO DA SAÚDE INFORMA: O ALEITAMENTO MATERNO EVITA INFECÇÕES E ALERGIAS E É RECOMENDADO ATÉ OS 2 (DOIS) ANOS DE IDADE OU MAIS.
				</div>
				<p>
					Goiasminas Indústria de Laticínios Ltda. © Todos os direitos reservados - SAC 08000 62 99 88
				</p>
				<p>
					©2018 italac.com.br <br>
					all right reserved
				</p>
			</div>
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<?php wp_footer(); ?>
<script>
	jQuery(document).ready(function() {	
		
		// jQuery(".simple-modal .close-modal, .modal-container-geral").on('click' , function(){
        //     jQuery(".modal-container-geral, .simple-modal").fadeOut();
        //     return false;
		// });
		
		// Menu dropdown
		jQuery(".menu-item-has-children").hover(function(){
			jQuery(this).addClass('current').find('.sub-menu').show();
		}, function(){
			jQuery(this).removeClass('current').find('.sub-menu').hide();
		});
		
		// Formulário de busca no topo
		jQuery(".header-search").on('click', function(e){
			e.preventDefault();
			jQuery(".header-search-inter").addClass("active");
		});

		jQuery(".header-search-inter .close-form").on('click', function(e){
			e.preventDefault();
			jQuery(".header-search-inter").removeClass('active');
		});

		// Redimencionando as imagens mantendo a proporção 1:1;
		function socialMediaResize(){
			var socialsW = jQuery('.socials-posts-item .foto a').width();
			jQuery('.socials-posts-item .foto a').height(socialsW);
		}
		
		jQuery(window).resize(function() {
			socialMediaResize();
		});

		//Inserir Imagem 
		jQuery(".unit-slide .metaslider .slides img").wrap('<div class="figure-unit" />');

		//Inserir classe ativa no menu produtos quando estiver na página interna
		jQuery(".single-produto #menu-menu-principal a:contains('Produtos')").parent().addClass('current-menu-item');


		//Desativar o link de produtos no breadcrumb
		var Nolink = jQuery(".bread_crumb a:contains('Produtos')");
		jQuery(Nolink).click(function(){
			jQuery(this).attr('href', '');
			return false;
		});

		jQuery(Nolink).on('hover', function(){
			jQuery(this).css({'text-decoration' : 'none', 'cursor': 'default'})
		});

		//Modal Unidades
		jQuery('.figure-unit img').wrap( '<a class="fancybox" href="" rel="gallery1"></a>' );

		jQuery( ".figure-unit img" ).each(function() {
			var imgActive = jQuery(this).attr('src');
			if ( jQuery(this).is( ".slide-1961" ) ) {
				jQuery(this).parent().attr('href', 'https://www.italac.com.br/wp-content/uploads/2020/12/pop-certificacao.png');
			}else{
				jQuery(this).parent().attr('href', imgActive);
			}
			console.log(imgActive);
		});



		jQuery(".fancybox").fancybox();

		//Tagueamento de novidades/saudabilidades
		jQuery(".type-post .news-btn").on('click', function(e){
			var titleNews = jQuery(this).parent().parent().find('h3').text();
			jQuery( ".type-post" ).each(function( index, element ) {
				if ( jQuery( this ).is( ".category-novidades" ) ) {
					_gaq.push(['_trackEvent', 'Novidades', 'Listagem de Novidades', 'Acessou ' + titleNews]);
					
				} else if( jQuery( this ).is( ".category-saudabilidade" )){
					_gaq.push(['_trackEvent', 'Saudabilidade', 'Listagem de Saudabilidade', 'Acessou ' + titleNews]);
				}
			});
		});
			
		//Tagueamento do menu principal 
		jQuery("#menu-menu-principal a").on('click', function(e){
			var menuLink = jQuery(this).text();
			_gaq.push(['_trackEvent', 'Cabeçalho', 'Menu Principal', 'Acessou ' + menuLink]);
		});

		//Tagueamento do menu principal 
		jQuery(".printomatic").on('click', function(e){
			var titleReceitas = jQuery(this).parent().find("h1").text();
			_gaq.push(['_trackEvent', 'Receitas', 'Imprimiu', titleReceitas]);
		});
	});
</script>
</body>
</html>

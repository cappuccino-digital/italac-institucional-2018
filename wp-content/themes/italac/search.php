<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) : ?>
			<div class="search-section section-main">
				<div class="container">
					<h1>
						<?php printf( __( 'Resultados da busca por: %s', 'Italac' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?>
					</h1>
					<div class="row search-box">
						<?php
							// Start the loop.
							while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/content', 'search' );

							// End the loop.
							endwhile;

							// Previous/next page navigation.
							the_posts_pagination( array(
								'prev_text'          => __( '&laquo;', 'Italac' ),
								'next_text'          => __( '&raquo;', 'Italac' ),
								'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'Italac' ) . ' </span>',
							) );

						// If no content, include the "No posts found" template.
						else :
							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
					</div>
				</div>
			</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->
<?php get_footer(); ?>

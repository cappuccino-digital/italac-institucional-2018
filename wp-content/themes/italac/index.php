<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

get_header(); ?>

	<div id="primary" >
		<div class="home-page">
			<!-- Slide Home -->
			<div class="banner-home">
				<?php echo do_shortcode("[rev_slider alias='slider-home']"); ?>
			</div>

			<!-- Products Section -->
			<div class="products-section section-main">
				<div class="container">
					<h2><span>Nossos</span> Produtos</h2>
					<div class="slider-products">
						<?php echo do_shortcode("[rev_slider alias='products-carrousel']"); ?>
					</div>
				</div>
			</div>
			
			<!-- Recipes Section -->
			<div class="recipes-section section-main" style="background: url('<?php get_site_url() ?>/wp-content/uploads/2018/08/recepes-bg.jpg') no-repeat fixed;">
				<div class="container">
					<h2>Receitas</h2>
					<div class="row recipes-box">
						<?php $loop = new WP_Query( array( 'post_type' => 'receitas', 'posts_per_page' => 4 ) );?>
						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<div class="col-3 item-recipes">
							<div class="figure-recipes" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg););"></div>
							<div class="content-info">
								<h3><?php the_title(); ?></h3>
								<ul class="icons-recipes">
									<li class="difficulty-fild">
										<strong>Dificuldade</strong>
										<?php the_field('dificuldade'); ?>	
									</li>
									<li class="preparation-fild">
										<strong>Preparo</strong>
										<?php the_field('tempo'); ?>
									</li>
									<li class="yield-fild">
										<strong>Rendimento</strong>
										<?php the_field('porções'); ?>
									</li>
								</ul>
								<div class="news-btn">
									<a href="<?php the_permalink() ?>" class="btn btn-blue" onclick="_gaq.push(['_trackEvent', 'Home', 'Receitas', 'Acessou <?php the_title(); ?>']);">Ver receita</a>
								</div>
							</div>
						</div>
						<?php 
							endwhile;
							wp_reset_postdata();
						?>
					</div>
					<a href="receitas/" class="btn btn-blue"  onclick="_gaq.push(['_trackEvent', 'Home', 'Receitas', 'Acessou Conheça todas as receitas']);">Conheça todas as receitas</a>
				</div>
			</div>

			<!-- News Section -->
			<div class="news-section section-main">
				<div class="container">	
					<h2>Novidades</h2>


					<div class="row news-box">
						<?php
							global $post;
							$args = array( 'numberposts' => 4, 'cat' => 1 );
							$myposts = get_posts( $args );
							foreach( $myposts as $post ) : setup_postdata($post); 
						?>
						<div class="col-3 news-item">
							<article>
								<div class="image-news" style="background-image: url(<?php the_post_thumbnail_url(); ?>), url(<?php get_site_url() ?>/wp-content/uploads/2018/09/Italac.jpg);"></div>
								<div class="content-new">
									<h3><?php the_title(); ?></h3>
									<div class="news-btn">
										<a href="<?php the_permalink() ?>" class="btn btn-white" onclick="_gaq.push(['_trackEvent', 'Home', 'Novidades', 'Acessou <?php the_title(); ?>']);">Ver notícia</a>
									</div>
								</div>
							</article>
						</div>
						<?php endforeach; ?>
					</div>
					<div class="text-center">
						<a href="novidades/" class="btn btn-blue"  onclick="_gaq.push(['_trackEvent', 'Home', 'Novidades', 'Acessou Veja mais notícias']);">Veja mais notícias</a>
					</div>
				</div>
			</div>
			
			<!-- Redes Socias -->
			<div class="social-section section-main">
				<div class="container">
					<h2><span>Nossas</span> Redes <i class="face"></i><i class="insta"></i></h2>

					<div class="socials-posts row">
						<div class="socials-posts-item col-3">
							<div class="foto">
								<a href="https://www.instagram.com/p/CH0EPKCgbRT/" style="background-image: url('<?php echo get_site_url(); ?>/wp-content/themes/italac/images/insta-queijo.jpg'); background-size: cover;" target="_blank">
								<img src="<?php echo get_site_url(); ?>/wp-content/themes/italac/images/blank-square.gif">
								</a>
							</div>
						</div>
						<div class="socials-posts-item col-3">
							<div class="foto">
								<a href="https://www.instagram.com/p/CIjTdYBFDyn/" style="background-image: url('<?php echo get_site_url(); ?>/wp-content/themes/italac/images/insta-minitorta.jpg'); background-size: cover;" target="_blank">
								<img src="<?php echo get_site_url(); ?>/wp-content/themes/italac/images/blank-square.gif">
								</a>
							</div>
						</div>

						<div class="socials-posts-item col-3">
							<div class="foto">
								<a href="https://www.facebook.com/ItalacOficial/posts/1320395838292980" style="background-image: url('<?php echo get_site_url(); ?>/wp-content/themes/italac/images/face-arroz.jpg'); background-size: cover;" target="_blank">
								<img src="<?php echo get_site_url(); ?>/wp-content/themes/italac/images/blank-square.gif">
								</a>
							</div>
						</div>

						<div class="socials-posts-item col-3">
							<div class="foto">
								<a href="https://www.facebook.com/ItalacOficial/posts/1309582036041027" style="background-image: url('<?php get_site_url() ?>/wp-content/themes/italac/images/face-escondidinho.jpg'); background-size: cover;" target="_blank">
								<img src="<?php echo get_site_url(); ?>/wp-content/themes/italac/images/blank-square.gif">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal 
		<div class="modal-container-geral" style="position: fixed; top:0; left:0; right:0; bottom:0; height:100%; width:100%; background: rgba(19,55,125,.8); z-index: 9999999;">
		</div>
		<div class="simple-modal" style="max-width: 1280px; margin: 0 auto; position: absolute; left: 0; right: 0; top: 100px; z-index:99999999;">
			<a class="close-modal" style="font-family: Arial; font-size: 20px; position: absolute;right: 0;background: #FFF;color: #14387c;padding: 20px;display: inline-block;font-weight: bold;
				cursor: pointer;">X</a>
			<img src="http://beta.italac.com.br/wp-content/uploads/2020/03/popup-corona.jpg" alt="" style="max-width: 100%" />
        </div>-->
<?php get_footer(); ?>


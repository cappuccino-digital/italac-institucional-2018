<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Italac
 * @since Italac 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<header class="page-header" style="background-image: url(<?php get_site_url() ?>/wp-content/uploads/2018/09/banner-recipes.jpg;">
				<div class="container">
					<h1>Produtos</h1>
				</div>
			</header><!-- .page-header -->
			
			<?php if ( have_posts() ) : ?>
			<div class="recipes-section section-main">
				<div class="container">
					<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?> 
					<div class="row prod-box">
						<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'produtos' );
						// End the loop.
						endwhile;
						else :
							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
					</div>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>
